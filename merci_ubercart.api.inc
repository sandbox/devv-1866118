<?php
/*
 * @file
 * Implements merci_ubercart's hook_modify_reservation_price().
 * Return a value that will be added to the cost of the node when created
 * This is the main code and is called by merci_ubercart_get_reservation_price
 */

function merci_ubercart_modify_reservation_price($node) {
  static $saved_value;
  if (isset ($saved_value)) {
    return $saved_value;
  }
  //get number of hours of reservation
  $date = $node->field_merci_date['und'][0];
  $start = date_create($date['value']);
  $finish = date_create($date['value2']);
  $interval = $start->diff($finish);

  //create an interval of those hours
  $days = $interval->format("%a"); //*total* number of days
  $hours = 24 * $days + $interval->format("%h");
  $cost = 0;

  //deal with each content type
  //TODO: are we dealing with buckets properly?
  foreach ($node->merci_reservation_items as $merci_choice) {
    //some aren't set
    if (empty($merci_choice['merci_item_nid'])) continue;

    if (is_numeric($merci_choice['merci_item_nid'])) {
      //there exists an actual node
      $merci_item = node_load($merci_choice['merci_item_nid']);
      $type = $merci_item->type;
    }
    else {
      //using a bucket
      $type = $merci_choice['merci_item_nid'];
    }

    //get the merci node type information with some MERCI function
    $settings = merci_load_item_settings($type);
    $actual_item_hours = $hours - $settings->merci_fee_free_hours;
    $per_hour = $settings->merci_rate_per_hour;
    $cost += $per_hour * $actual_item_hours;
  }//for

  $saved_value = $cost;
  return $cost;
}
